import java.text.DecimalFormat;
import java.text.NumberFormat;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double a, b, c, d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {

      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {

      NumberFormat plusMinus = new DecimalFormat("+#;-#"); // Change NumberFormat to retain + signs

      StringBuilder builder = new StringBuilder();

      builder.append(plusMinus.format(this.a));
      builder.append(plusMinus.format(this.b));
      builder.append(plusMinus.format(this.c));
      builder.append(plusMinus.format(this.d));

      return builder.toString();
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {

      String[] values = s.split("(?<=[0-9])(?![0-9])");  // Split on every zero-length string preceded by a number and not followed by a number

      // Quaternion requires exactly 4 parameters
      if (values.length != 4) throw new IllegalArgumentException("String does not represent a quaternion - too many parameters in: " + s);

      Double[] ret = new Double[values.length];

      for (int i = 0; i < values.length; i++) {
         try {
            ret[i] = Double.parseDouble(values[i]);
         } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("String does not represent a quaternion - parameters have to be digits: " + s);
         }
      }

      return new Quaternion(ret[0], ret[1], ret[2], ret[3]);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Quaternion(this.a, this.b, this.c, this.d);

   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

      return equals(new Quaternion(0., 0., 0., 0.));
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {

      return new Quaternion(this.a, -this.b, -this.c, -this.d);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {

      return new Quaternion(-this.a, -this.b, -this.c, -this.d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {

      double r = this.a + q.a;
      double i = this.b + q.b;
      double j = this.c + q.c;
      double k = this.d + q.d;

      return new Quaternion(r, i, j, k);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {

      double r = this.a*q.a - this.b*q.b - this.c*q.c - this.d*q.d;
      double i = this.a*q.b + this.b*q.a + this.c*q.d - this.d*q.c;
      double j = this.a*q.c - this.b*q.d + this.c*q.a + this.d*q.b;
      double k = this.a*q.d + this.b*q.c - this.c*q.b + this.d*q.a;

      return new Quaternion(r, i, j, k);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {

      double rv = this.a * r;
      double i = this.b * r;
      double j = this.c * r;
      double k = this.d * r;

      return new Quaternion(rv, i, j, k);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {

      if(this.isZero()) throw new RuntimeException("Division by zero not allowed");

      double x = (
              this.a*this.a +
              this.b*this.b +
              this.c*this.c +
              this.d*this.d
      );

      double r = this.a/x;
      double i = -this.b/x;
      double j = -this.c/x;
      double k = -this.d/x;

      return new Quaternion(r, i, j, k);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {

      double r = this.a - q.a;
      double i = this.b - q.b;
      double j = this.c - q.c;
      double k = this.d - q.d;

      return new Quaternion(r, i, j, k);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {

      if(q.isZero()) throw new RuntimeException("Division by zero not allowed");

      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {

      if(q.isZero()) throw new RuntimeException("Division by zero not allowed");

      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {

      Quaternion temp = (Quaternion)qo;

      double delta = 0.000001;

      if (Math.abs(this.a - temp.a) > delta) return false;
      else if (Math.abs(this.b - temp.b) > delta) return false;
      else if (Math.abs(this.c - temp.c) > delta) return false;
      else if (Math.abs(this.d - temp.d) > delta) return false;
      else return true;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {

      Quaternion ret = times(q.conjugate()).plus(q.times(conjugate()));

      return new Quaternion(ret.a/2, ret.b/2, ret.c/2, ret.d/2);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {

      return toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {

      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {

      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
